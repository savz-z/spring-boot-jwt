CREATE TABLE user (
  id mediumint NOT NULL AUTO_INCREMENT,
  user_name varchar(50) NOT NULL,
  user_pwd varchar(255) NOT NULL,
  first_name varchar(50) DEFAULT NULL,
  last_name varchar(50) DEFAULT NULL,
  user_email varchar(50) DEFAULT NULL,
  user_contact mediumint DEFAULT NULL,
  user_createdAt date DEFAULT NULL,
  user_modifiedAt date DEFAULT NULL,
  PRIMARY KEY (id,user_name),
  UNIQUE KEY user_name (user_name)
) ENGINE=InnoDB;

CREATE TABLE user_role (
  id mediumint NOT NULL AUTO_INCREMENT,
  user_name varchar(50) NOT NULL,
  user_role varchar(50) NOT NULL,
  role_createdAt date DEFAULT NULL,
  PRIMARY KEY (id),
  KEY user_name (user_name),
  CONSTRAINT user_role_ibfk_1 FOREIGN KEY (user_name) REFERENCES user (user_name) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE movie (
  id mediumint NOT NULL AUTO_INCREMENT,
  movie_name varchar(50) NOT NULL,
  release_year mediumint DEFAULT NULL,
  release_date date NOT NULL,
  movie_director varchar(50) DEFAULT NULL,
  movie_synopsis varchar(8000) DEFAULT NULL,
  movie_path varchar(255) DEFAULT NULL,
  movie_trailer_url varchar(255) DEFAULT NULL,
  movie_producer varchar(255) DEFAULT NULL,
  movie_duration mediumint DEFAULT NULL,
  movie_writer varchar(255) DEFAULT NULL,
  PRIMARY KEY (id,movie_name),
  UNIQUE KEY movie_name (movie_name)
) ENGINE=InnoDB;

CREATE TABLE actor (
  id mediumint NOT NULL AUTO_INCREMENT,
  actor_name varchar(50) NOT NULL,
  birth_date date DEFAULT NULL,
  actor_desc1 varchar(255) DEFAULT NULL,
  actor_desc2 varchar(255) DEFAULT NULL,
  actor_desc3 varchar(255) DEFAULT NULL,
  actor_desc4 varchar(255) DEFAULT NULL,
  actor_desc5 varchar(255) DEFAULT NULL,
  actor_path varchar(255) DEFAULT NULL,
  PRIMARY KEY (id,actor_name),
  UNIQUE KEY actor_name (actor_name)
) ENGINE=InnoDB;

CREATE TABLE actor_movie (
  actor_id mediumint NOT NULL,
  movie_id mediumint NOT NULL,
  PRIMARY KEY (actor_id,movie_id),
  KEY movie_id (movie_id),
  CONSTRAINT actor_movie_ibfk_1 FOREIGN KEY (actor_id) REFERENCES actor (id) ON DELETE CASCADE,
  CONSTRAINT actor_movie_ibfk_2 FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE movie_feedback (
  id mediumint NOT NULL AUTO_INCREMENT,
  movie_name varchar(50) NOT NULL,
  feedback_desc varchar(255) DEFAULT NULL,
  user_name varchar(50) DEFAULT NULL,
  feedback_rating mediumint DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;




