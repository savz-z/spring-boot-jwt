package com.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieBootApp {

	public static void main(String[] args) {
        SpringApplication.run(MovieBootApp.class, args);
    }
}
